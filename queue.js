let collection = [];

// Write the queue functions below.

const print = () => {
  return collection;
};

const enqueue = (element) => {
  const size = collection.length;
  collection[size] = element;
  return collection;
};

const dequeue = () => {
  const newArray = [];
  let counter = 0;
  delete collection[0];
  for (let i = 0; i < collection.length; i++) {
    if (typeof collection[i] !== 'undefined') {
      newArray[counter] = collection[i];
      counter += 1;
    }
  }
  collection = newArray;
  return collection;
};

const front = () => {
  return collection[0];
};

const size = () => {
  return collection.length;
};

const isEmpty = () => {
  return collection.length === 0;
};

module.exports = { print, enqueue, dequeue, front, size, isEmpty };
